# Home

If you are a student, you are probably looking for [the accompanying wiki](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/wikis/home).

If you are an instructor (and I'm talking to myself here), see `Instructors/README.md
