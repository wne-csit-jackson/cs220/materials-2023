# Merging Part 1

## Roles

Work in pairs or triples. One or more of your team members will have
multiple roles.

* Manager
* Driver
* Recorder
* Skeptic

## Model 1 - `git-merging-1.zip`

Time: 20m

1. `git-merging-1.zip` contains a project with a git repository.
    Unzip it and open a command-line positioned in the root of the `git-merging` folder.

    > **IMPORTANT**
    >
    > On Windows, be sure to right click on the zip file and extract it.
    > ***Do not*** simply double-click it. That only allows you to browse its
    > contents, but does not extract its contents.

2. Open the root of the project in VS Code, and open a terminal within VS Code.

3. Being careful not to change the contents or state of the repository, use
   `git` commands and the VS Code file browser to inspect the state of the
   repository. In particular use the following git commands: `git status`,
   `git log --all --graph --oneline`, `git branch`, etc.).

4. Answer the following questions:

   1. How many branches are there? Name them.
   2. Which branch is active? How do you know?
   3. How many files and folders are there? Name them?
   4. Summarize the graph structure of the commits.

5. Run the following command, note its output, and display the commit graph.
   What has changed and why?

    ```bash
    git merge main
    ```

6. Switch to the branch `foo`. Inspect the files and summarize your observations.
   Compare them to those in 4.

7. On branch `foo`, run the following command, note its output,
   and display the commit graph. What has changed and why?

   ```bash
   git merge main
   ```

8.  Switch to the branch `main`, then run the following command, note its output,
    and display the commit graph. What has changed and why?

    ```bash
    git merge foo
    ```

9.  What type of merge was performed in the previous step?

10.  Inspect the files and commit graph and summarize your observations.

11. Explain what a **fast-forward merge** does.

12. Switch to branch `foo`. Inspect the files and the commit graph.
    Summarize your observations.

13. Which branch was modified by step 8?

14. Assume you are working in a different project.
    Assume all of the branches named below exist.
    Which branch will be modified by the following sequence?

    ```
    git switch branch1
    git merge branch2
    ```

15. Complete the following statement.

    > To merge the contents of branch A into branch B, you must first switch to...

16. Let's delete the `foo` branch.
    Use the command below.
    You will probably get an error.
    Read it and try to figure out what you need to do first before you can
    delete a branch.

    ```
    git branch -d foo
    ```

## Model 2 - `git-merging-2.zip`

Time: 15m

Your goal is to merge `main` into `foo`.

1. Download and extract `git-merging-2.zip`, and open it in VS Code and
    open a terminal in VS Code.
2. Inspect the project.
3. Formulate a plan. Write a command sequence below that you think will
   merge `main` into `foo`.
4. Execute your plan.
    If you are asked for a commit message, use the default provided message.
5. What type of merge was performed?
6. Inspect the state of the files and the repository.
   1. Was a new commit created?
   2. Which branch contains the new commit?
7. Switch to and inspect `main`. Has it changed?
8. Explain what an "ort" merge does.
9. Search for what "ort" means. Prepare to facepalm.
10. If you have time and interest, start this section over and merge `foo`
    into `main` instead.

## Model 3 - `git-merging-3.zip`

Time: 10m

Download and extract `git-merging-3.zip`.

One more time. This time, merge `intro` into `main`.
Then merge `main` into `conclusion`.
Then merge `conclusion` into `main`.

As you do, pay attention to the contents of README.md, the commit graph
structure, and the types of commits. Try to predict what is about to
happen before you do it.

Summarize your observations.

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
