# Docker - Managing Images and Containers

Objectives

- Manage images and containers with Docker.

## Model 0

[Short? Presentation](https://docs.google.com/presentation/d/1adFGr3P5U7P49KcDw1sgNfAhiKwGOvNmiDOPbRnc3gM/edit?usp=sharing)

1. Durning the presentation, take notes.

2. After the presentation, review the notes with your team and identify questions.


(class discussion)


## Model 1

Objectives

- Ensure Docker Desktop is running.

Instructions

1. Look for the Docker logo in your system tray. If it is there and static (not animating). Move on to next model.

2. Otherwise, start Docker Desktop and wait for this icon to stop animating. Then move on to next model.

## Model 2

Objectives

- Able to download images.
- Able to list available local images.

Run and observe.

```
docker images
docker pull busybox
docker images
```

Answer based on your observations.

1. What does `docker images` do?

2. What does `docker pull busybox` do?

3. What tag does `docker pull` use by default?

4. Download the latest `mongo` image. What command did you run?

5. Confirm that your system has the latest `mongo` image. What command did you run?


## Model 3

Objectives

- Remove images

Run and observe.

```
docker images
docker rmi busybox
docker images
```

Answer based on your observations.

2. What does `docker rmi busybox` do?

3. What do you think `rmi` is short for?

4. Remove the `mongo` image. What command did you run?

5. Confirm the `mongo` image is gone. What command did you run?


## Model 4

Objectives

- Explain what Docker does if you pull an image you already have locally.

Run and observe.

```
docker images
docker pull mongo
docker images
docker pull mongo
```

Answer based on your observations.

1. What did the second `docker pull mongo` do?

2. What do you think would have happened if `mongo` was not "up to date"?

3. What command do you run to update your local copy of `busybox`?

## Model 5

Objectives

- Remove all images.

Instructions

1. Use what you have learned and remove all images.

## Model 6

Objective

- Run a docker image.

Run and observe.

```
docker ps
docker run --detach nginx
docker ps
```

Answer based on your observations.

1. Based on your observations, what does `docker ps` do?

2. Based on your observations, what does `docker run --detach nginx` do?

3. In the output of the second `docker ps`...
    - What is the ID of the container?
    - What image was the container created from?
    - What is the name of the container?

4. How might you run a `mongo` container?


## Model 7

Objectives

- Stop a container
- List stopped containers

Run and observe.

*Replace ID with the container id identified in the previous model.*
```
docker ps
docker stop ID
docker ps
docker ps -a
```

Answer based on your observations.

1. What does `docker stop ID` do?

2. What does `docker ps -a` do?

3. What do you think `-a` stands for?


## Model 8

Objectives

- Start a stopped container

Run and observe.

*Replace NAME with a container name of the stopped nginx container.*
```
docker ps -a
docker start NAME
docker ps
```

Answer based on your observations.

1. What changed in the output of `docker ps` after `docker start NAME`?

2. Do you think you could have used the container ID to start the container?

3. Do you think you could have used the container NAME to stop the container in the previous model?

## Model 9

Objectives

- Remove a container.

Run and observe.

*Replace NAME with your container's name.*
```
docker run --detach nginx
docker ps -a
docker rm NAME_OR_ID
docker stop NAME_OR_ID
docker ps -a
docker rm NAME_OR_ID
docker ps -a
docker images
```

Answer based on your observations.

1. Which command removes a container?

2. What must be true before you can remove a container?

3. Does removing a container remove the image?

## Model 10

Objectives

- Run multiple containers from the same image.

Run and observe.

```
docker run --detach nginx
docker run --detach nginx
docker ps
```

Answer based on your observations.

1. How many nginx containers are running?

2. How would you describe the relationship between images and containers?


## Model 11

Objective

- Learn to cleanup

Instructions

1. Start two containers. Then run the following command. What does it do?

    ```
    docker ps -q
    ```

2. What does the following command do? How are the two commands working together?

    ```
    docker stop $(docker ps -q)
    ```

3. What does the following command do?

    ```
    docker system prune
    ```

4. Did the last command delete your local images?

5. Based on the previous commands and other commands you know, figure out a one-line command to delete all local images. Write the command below.

## Model 12

TIP: If you want to clean up all the Docker stuff...

```bash
docker ps -aq | xargs docker rm -f
docker system prune --all --volumes
```

Don't do this on a production system, but great for a development system.

---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
