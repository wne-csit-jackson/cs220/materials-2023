## Docker: Managing Images and Containers

* [Raw](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/raw/main/Activities/Docker/Docker-Managing-Images-and-Containers-Template.md)
* [Rendered](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/blob/main/Activities/Docker/Docker-Managing-Images-and-Containers-Template.md)

To get started:

1. Fork https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class-ro/docker into your team's group.
2. Clone your fork.
3. With Docker running, open the root of your clone in VS Code and allow it to
    reopen in a devcontainer.

Enjoy!

## Team Notes

* 
