# Docker Compose

Docker commands are filled with configuration information.

* `--name` allows us to name the container within our docker engine.
* `--mount` tells Docker where we want to store data and configuration
    files and where to make them available inside the container.
* `-p` tells Docker which container ports should be made available outside
    the container and allows us to choose the port number that it should
    be mapped to.
* `-e` allows us to pass in environment vairables to further configure
    the run of a container. `--env-file` helps by allowing us to store
    these variables in a file and then pass that file to Docker.
* When we specify the image we want to create the container from, we can
    specify the registry to use and the version number we want.

That's a lot to remember. Instead we should codify this somewhere.
Here are a couple of options.

* Document the exact commands in a README.md.
* Create a set of scripts that encode the commands.

Scripts are certainly better than documentation. But what happens when
we start building systems that are composed of more that one container?
Maybe we'll have one container that runs a webserver, another that runs
a database, and one that implements a REST API. Such systems have become
increasingly popular for good reasons with the advent of the [Microservice
Architecture](https://martinfowler.com/microservices/).

Also, when we move from one project to another, will the scripts be the
same? Will we interact with the scripts in the same way? Will their
workflow be the same? Maybe, maybe not. So we'll need to document their
use in a README.md and developers will need to carefully read this
documentation to understand their use.

We can do better with help from Docker Compose. Docker Compose is written
by the same organization that brought you Docker. To use it, you write
a compose file, written in YAML, and then use either the standalone
`docker-compose` command or the embedded docker subcommand `docker compose`
(notice the lack of a hyphen). The `docker compose` is the way of the
future. I recommend using it unless it doesn't work for you, then use
`docker-compose`.

## YAML

Compose files are written in YAML. YAML stands for "Yet Another Markup
Language". It's meant as an alternative to JSON with lighter weight
syntax. YAML is whitespace sensitive -- meaning indentation matters.

When writing YAML, its helpful to understand the types/structures that
you are encoding. Below are some examples and their JSON equivalent.
Why JSON? Because JSON's structure is more explicit.

```yaml
# YAML has comments, JSON does not.
field1: one
field2:
    sub1: 1
    sub2: True
    sub3: Three
field3:
    - first
    - second
    - third
field4:
    - subA: a
      subB: b       # note indentation
      subC: c       # note indentation
    - Simple
```

```json
{
    "field1": "one",
    "field2": {
        "sub1": 1,
        "sub2": True,
        "sub3": "Three"
    },
    "field3": [
        "first",
        "second",
        "third"
    ],
    "field4": [
        {
            "subA": "a",
            "subB": "b",
            "subC": "c"
        },
        "Simple"
    ]
}
```

1. What does YAML have that JSON doesn't?

2. What are the types of values available in both YAML and JSON?

3. Do you think JSON is whitespace sensitive?


## Model - busybox

### Plain Docker

```
docker run --name my-busybox --detach --mount type=bind,src=${PWD},dst=/workspace -e CAT -e DOG=Fido busybox
docker ps --all
docker stop my-busybox
docker rm my-busybox
```

### Docker Compose

Compose file

```yaml
# compose.yaml
services:
    my-busybox:
        image: busybox
        environment:
            CAT:
            DOG: Fido
        volumes:
            - type: bind
              source: .             # note indentation
              target: /workspace    # note indentation
```

Equivalent Commands

```
docker compose run -i --detach my-busybox
docker compose ps --all
docker compose stop my-busybox
docker compose rm my-busybox
```

1. Try the above setup.

2. What questions do you have?

## nginx challenge

1. Find the official documentation for compose files.

2. Write a `compose.yaml` based on the following `docker run` command.

    ```
    docker run --detach --name devweb -p 8080:80 --mount type=bind,src=${PWD}/src,dst=/usr/share/nginx/html -e NGINX_HOST=pets.org -e NGINX_PORT=80 nginx:1.21
    ```

3. Test it. You'll need a ./src directory with an index.html file.

4. Stop and remove the container.

5. Try these additional commands

    ```
    docker compose up --detach
    docker compose --all
    docker compose down
    docker compose --all
    ```

6. Update your scripts in bin/ to use `docker compose` commands.
   Is this useful?

6. What questions do you have?

7. Summarize what you have learned.
