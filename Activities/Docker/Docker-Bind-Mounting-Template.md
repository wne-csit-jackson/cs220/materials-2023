# Docker - Bind Mounting

Within the Docker-in-Docker devcontainer provided by your instructor...

## Model 1

1. Run the following commands. Compare the results of uname, ls, pwd, and
    whoami before and after `docker run`.
    Then try to explain what happened.

    ```
    uname
    ls
    pwd
    whoami

    docker run -it --rm busybox

    uname
    ls
    pwd
    whoami

    exit
    ```

2. Run the following commands (expect explosion), and explain the results.

    ```
    docker run -it --rm --mount type=bind,src="${PWD}/my-work.txt",dst="/workdir" busybox
    ```

3. Run the following commands, and explain the results.

    ```
    echo "Hi" > my-work.txt
    ls
    cat my-work.txt
    docker run -it --rm --mount type=bind,src="${PWD}/my-work.txt",dst="/my-work.txt" busybox
    ls
    cat my-work.txt
    ```

4. Run the following commands, and explain the results.

    ```
    echo "bye" > my-work.txt
    cat my-work.txt
    exit
    cat my-work.txt
    ```

5. Run the following commands, and explain the results.

    ```
    mkdir my-files
    mv my-work.txt my-files
    ls
    docker run -it --rm --mount type=bind,src="${PWD}/my-files",dst="/workdir" busybox
    ls
    cat /workdir/my-work.txt
    exit
    ```

6. What have you learned?


---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
