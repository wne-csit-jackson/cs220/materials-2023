# Activity - Docker Building Images

Use our Docker-in-Docker container for this activity.

## Overview

One common use of Docker is to package up an application for easy, repeatable
installation and use.

Another use of Docker is to create an image that contains all the tools necessary
to build, test, and run an application &emdash; i.e., a development environment.

Why would you do this? For one, you may use your computer to work on multiple
projects. What happens when the development dependencies conflict with those
for another project (e.g., one needs JDK 11 and another JDK 13)? You may end up
breaking one environment when installing or updating another environment.

Another reason to create a Docker image containing a development environment is
that you will know exactly what is needed to build, test, and run that application.
When working on a project on your native system, it's sometimes too easy for a
tool to become a dependency of your development environment, sometimes without
you knowing it. When this happens, you'll probably forget to document it. Then
if you or another developer tries to work on the project on a different machine,
they'll be missing the dependency and will struggle to get the project working again.

Having a Docker image that contains all of the development dependencies and
their correct versions installed is critical for creating a repeatable development
environment for a project.

In this activity, you'll create a development environment for a REST-API service.

## Model 1

Create a directory to work in.

```
mkdir devenv
cd devenv
```

## Model 2

Create an empty Dockerfile.

```
touch Dockerfile
```

## Model 3

Select an image to base our image on.

Most of the time you don't start from scratch. Instead you start from an image that
someone else built. You pick an image based on what it has installed already, and
what you want to be able to install. Also, we often want our resulting image to be
as small as possible; so we want to start with a small image to begin with.

A common starting image is `alpine`: <https://hub.docker.com/_/alpine>. It provides
a basic set of common Linux commands and a package index that will allow us to
install other common packages, and is only 5 MB!!!

It is also a good idea to specify the exact version of every dependency you put
into your Dockerfile. This will help ensure that over time, your Dockerfile
will be able to provide a stable development environment. If you ever want to
upgrade a dependency you will do so intentionally and with an appropriate
amount of testing to ensure that the new image still works. When starting a new
project, I look on <https://hub.docker.com/_/alpine> for the most specific tag
that the `latest` tag is associated with. At this time of writing, that's
`3.15.4`. To ensure this activity works as expect, we'll use that tag for `alpine`.

Normally, to ensure repeatability, we'd use the complete most specific tag.
However, Alpine only keeps around the most recent patch release for each MAJOR.MINOR
release. In other words, when `3.15.5` comes out, `3.15.4` will cease to exist.
(Note that previous minor versions will continue to exist: e.g., 3.14.x, 3.13.x, ...)
So to ensure that our Dockerfile will work in the future (a.k.a. future-proofing),
we will use `3.15` instead. This will pull the most recent patch version of `3.15`.

Add the follow as the first line of your `Dockerfile`

```
FROM alpine:3.15
```

Save your file.

## Model 4

Let's test it by building our first image. Let's tag our image
`devenv:latest`. The dot `.` in the following command is important. It tells
Docker that the current directory is the build context and contains the
Dockerfile to build our image.

```
docker build -t devenv:latest .
```

## Model 5

Let's test the image that it built by running a container based on it.

```
docker run -it --rm devenv:latest
```

By default, alpine runs `/bin/sh`. That is, it starts a Linux-style
Bourne Shell. So you should be looking at a command prompt that looks
something like `/ # `. Enter `exit` to exit, stop, and remove the container.

So we have an initial command-line development environment for our REST API
service. But there isn't much in there. Let's fix that.

## Model 6

The Bourne Shell is not as feature rich as other shells. So let's setup a
different shell: Bash. First we need to install Bash into our image. To
install common packages, we can use Alpine's package manager `apk`. To
install a package, we'll need to know its name, and for stability we'd also
like a specific version number.

A list of Alpine packages that are available for each Alpine version is
available at <https://pkgs.alpinelinux.org/packages> (if you forget, Google
`alpine packages`). When searching for a package, first set the version of
Alpine you are using. By default it probably reads `edge`. Change this to
`v3.15`. Then search for `bash`. Note that the name is in fact `bash`, and
copy the version number. At the time of writing it was `5.1.16-r0`. Again,
Alpine only keeps the most recent patch release for each MAJOR.MINOR release
for all its packages, so we'll use `5.1` when we install bash.

The following line runs the `apk` command inside the image to add the `bash`
package. The `--update` updates apk's index of available packages.
The `--no-cache` prevents `apk` from creating cache files that would
make future calls to `apk` faster, but would make our image larger. The `~=`
allows apk to find `bash` with approximately version `5.1`, allowing the
remaining components to vary. Add the following line after the `FROM` in
your Dockerfile.

```
RUN apk add --update --no-cache bash~=5.1
```

Save your Dockerfile. Rebuild your image. And run your container again.

```
docker build -t devenv:latest .
docker run -it --rm devenv:latest
```

Notice you are at the same prompt as before. The problem is that although
we have installed bash, the image is still starting Bourne Shell (`/bin/sh`)
by default. We'll fix that next. But first, let's make sure bash is installed
by running bash.

```
bash
```

Your prompt should change to `bash-5.1# `. That's good. Now exit bash by typing
`exit`. Then exit, stop, and remove your container by typing `exit` **again**.

## Model 7

Let's configure our image to run `bash` instead of `sh` when starting the container.
Add the following to your Docker file.

```
CMD [ "/bin/bash" ]
```

Rebuild and retest your image. This time you should get the `bash-5.1# ` prompt. Type
`exit` to exit, stop, and remove your container.

CMD is the default command to run when starting the container. You can override
this on the command-line when you run the container. For example, if you
would like to start in Bourne Shell you could run the following.

```
docker run -it --rm devenv:latest /bin/sh
```

Try it. Be sure to exit when you are done.

Notice that the `/bin/sh` replaces the `/bin/bash` in CMD. Of course this
is temporary. When you run a new container without `/bin/sh` on the end
the default CMD will run.

## Model 8

Our REST API service will be running a NodeJS (JavaScript) server. So we'll need to
use `apk` to install NodeJS. Use <https://pkgs.alpinelinux.org/packages> to find the
name and version number of NodeJS and add a line to your Dockerfile to install the
package in the image.

Rebuild and run your image. Test that `node` is available by running `node`. You should
see a welcome message from Node.js and a new prompt `> `. To exit node, type `.exit`.
Then exit your container as normal.

## Model 9

Most NodeJS applications will need to install 3rd party JavaScript libraries. Ours
will need mongo, express, and more. To install 3rd party JavaScript libraries, we
need a JavaScript package manager. There are two popular ones, `npm` and `yarn`. We'll
use `yarn`. Again update your Dockerfile to install yarn. Test that your image has yarn
installed by running `yarn --version` inside your container. You should see yarn's version
number. Exit your container when you are done.

## Model 10

Some tools/libraries may not be available in apk. These tools often provide
instructions for how to install them by downloading some archive file
and how to run it. For developer tools, these instructions are often given
as command-line instructions. This is great because they can be included
in our Dockerfile.

Let's say we want openapi-generator-cli. A tool written in Java to validate
OpenAPI specification files. Their website tells us to download it as follows:

```bash
wget -nv https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/5.4.0/openapi-generator-cli-5.4.0.jar -O /usr/lib/openapi-generator-cli.jar
```

(Don't worry, wget and its options are not on the exam.)

Then we can use it through Java like this `java -jar /usr/lib/openapi-generator-cli.jar`.

Let's get our image ready to work with openapi-generator-cli.

1. Use apk to install openjdk11. (confirm with `java --version`)
2. Make sure wget is installed into the image. (confirm with `wget --version`).
3. Then add a RUN command to your Dockerfile that runs that long `wget -nv ...`
    command above to download openapi-generator-cli.jar.

We need a 3rd party tool that is distributed as a Java JAR file that requires Java 11 to
run it. Install openjdk11 into the image. Test it by running `java --version`

Run the container and check that it contains the jar by running `ls -l /usr/lib/openapi-generator-cli.jar`.

## Model 11

We've built up a pretty good development environment. We're going to need a location inside
the container where we will mount the source code of the application we want to work on.
Let's assume we'll mount it to `/workdir`. When we run our container, we would like to
automatically be in the `/workdir` directory so we can immediately get to work. Add the
following to the end of your Dockerfile.

```
WORKDIR "/workdir"
```

Rebuild.

The above command will create the directory and change to it when the container starts.
When we run the container, we should bind mount the directory that contains our files
into `/workdir`. For example,

```
docker run -it --rm --mount "type=bind,src=$PWD,dst=/workdir" devenv:latest
```

Test by running `ls -a` and you should see your files (e.g., the Dockerfile). Also run `pwd`
and you should see that you are in `/workdir`.

## Model 13

Use `docker images` to see the size of your image.

Use `docker system prune` to remove untagged images (and other cruft).

You could now publish this image, or just give this Dockerfile to another developer, and
they can build and run the exact same development environment you just constructed. Docker FTW!

---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
