# Docker - NodePort Publishing

Use the same technique as in the last activity to start a terminal inside a
Docker-in-Docker VS Code DevContainer.

## Model 1 - Failed attempt

- Goal: Learn to run a webserver and make it accessible through a browser
  running on your host.
- Fact: Webservers use port 80/tcp by default (per HTTP).
- Fact: NGINX is a webserver, so it listens on port 80 by default.

1. Start an nginx server as follows.

    ```
    docker run --rm --detach nginx
    ```

2. nginx should be listening for requests on port 80.
   Run the command below. Is nginx running? Is it listening on port 80?

    ```
    docker ps
    ```

3. Try accessing the webserver from a browser on your host machine:
   <http://localhost:80/>. Why do you think this didn't this work?

4. Stop and remove the nginx container. What commands did you use?

5. Start a new nginx container adding the `-p` option.

    ```
    docker run --rm --detach -p 80 nginx
    ```

6. Confirm that it is running as you did in step 2. Compare the result with the
   result you got in step 2. In the port section you should see something like
   `0.0.0.0:49153->80/tcp`. What do you think that means?

7. Try opening a browser to <http://localhost:80/>. Why do you think this
   didn't it work?

>&#9888; WARNING: Browsers typically cache pages previously visited. If the
>page changes, to view the new contents, you'll need to refresh the page in the
>browser, or close the page/tab and reopen it.

8. Try opening a browser to <http://localhost:49153> but replace 49153 with the
   port number that you have on the left-hand side of the arror `->` in the
   `docker ps` listing. Why do you think this didn't work?

9. In VS Code's Ports tab, forward the port you tried in step 8.

10. Retry step 8. This time it should work. What did step 9 do?

11. Before continuing, stop and remove the container, and remove the forwarded
    port in VS Code.

12. Summarize what you have learned.

---

Pretty good. But what if we want to access our container from a specific port
of our hoosing rather than some random port Docker gives us? Let's see if we
can git it running on port 80.

1. Run an nginx container with a slightly different port option.

    ```
    docker run --rm --detach -p 80:80 nginx
    ```

2. Try accessing <http://localhost:80/>. It doesn't work. Why? Fix it.

3. Shut it all down.

4. Practice: Start an nginx server accessible on port 8080.

5. Summarize what you have learned.

---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
