## GitKit Activity

Your instructor will make the activity available to you as a Word document.
Have your recorder download and edit this document.
Upload your modified activity document here.

## Team Notes

Community and Collaboration

*

Working Locally and Upstreaming

*

Staying Synchronized

*

Merge Conflicts

*
