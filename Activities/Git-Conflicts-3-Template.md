# Part 3: Resolving Lexical Conflicts

## Roles

* Manager
* Driver
* Recorder
* Skeptic


## Instructions

1. Download and extract git-conflicts-3.zip. Open the root of the project in
   VS Code. Open a terminal in VS Code which is also positioned in the root
   of the project. Inspect the repository as you did in Part 1.
   Summarize your findings below.


2. Predict what you expect to happen when you merge the non-master branch
   into master.


3. Merge the non-master branch into master, but **abort** the merge.
   What command did you use to abort the merge? Inspect your repository.
   Summarize your findings.


4. Merge the non-master branch into master, resolving any conflicts.
   Inspect your repository. Summarize your findings.

## Practice: Create your own conflict

1. Create a new branch, and make some commits to that branch.
2. Go back to master, and make some commits---some of which that will conflict.
3. Merge the new branch into master and resolve the conflicts.

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
