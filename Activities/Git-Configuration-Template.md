# Git - Configuration

Before working with Git, you need to configure git. If you are on a personal machine, much of this configuration you will only need to do once. If you work on a public shared computer (as in a lab), you will need to do some of these each time you sit down to work, and clear them before you leave.

## Roles

Work in pairs.

* Recorder: INITIALS
* Driver: INITIALS

## Model 1 - User Information

Time: 10m

Open a terminal running a linux-based shell with access to git (e.g., Git-Bash, Terminal.app, iTerm.app, or the terminal in VS Code) and follow along.

> :rotating_light: Privacy Warning! Read this section carefully so you can make an informed decision.

Probably the most important configuration is your identity. Your name and email will be stored along each commit you make. This is how Git attributes authorship to you. This information is easily accessible to anyone who has access to your repository and its commits. It is common to push your commits to a public repository. If this ever happens, your name and email will be publicly available. So choose a name and email that will publicly represent you. Then give this information to Git as follows.

```bash
git config --global user.name "Your Name"
git config --global user.email "your.email@someservice.com"
```

This information is NOT used to authenticate you to Git or any other service. However, services like GitHub and GitLab will use this information to give you credit for your commit by looking for an account that uses the same email and name. So, whatever name and email you choose here, you will likely want to use when you create your GitLab or GitHub account.

You can verify your settings by repeating the commands without a value.

```bash
git config --global user.name
git config --global user.email
```

You should see the values you entered before.

If you are working in a public lab, you should clear these values before you walk away. One way you can do this is by setting them to some arbitrary value.

>:information_source: Info: The `--global` sets this configuration for every repository **you** create on the current machine. Alternatively, if you replaced this with `--system` it would set this configuration for the entire system; and if you left it off, it sets it for the current repository.

Configure your `git` with your identity and verify that it has the right values now.

## Model 2 - Help

Demo: 5m

Git comes with built in documentation. To get detailed documentation on a command, enter `git help COMMAND` where `COMMAND` is the command you are interested in. To get more information about `config`, enter the following:

```bash
git help config
```

This documentation is so long that git is using a "pager" application (usually `less`) to display the documentation a page at a time. Many command-line applications use this pager, so it's worth learning how to navigate it. To move a page forward, press the spacebar. To move a page back, press `b`. To search for a word type `/someword` and then `n` to step through each occurrence. To exit, type `q`.

In Linux, you can get help on most top-level commands like `less` and `git` using the `man` command (short for "manual"). For more information about `less`, try `man less`. Or for more information on `man` itself: `man man`. Note that `man` also uses `less` to page its results. Git it a try now.

## Model 3 - Default Branch

Demo: 5m

Branches are used to track a chain of commits. The default branch has traditionally been named `master`, and much of the documentation on the Web currently refers to this name. However, many including GitHub (owned by Microsoft) are switching to use the more inclusive term `main`, and Git now makes it easy to set the default branch name for any repositories we create. Let's make this change now.

Confirm that your git installation is configure to use `main` as the default branch.

```bash
git config --global init.defaultbranch
```

If the result did not say `main`, set it to main as follows.

```bash
git config --global init.defaultbranch main
```

## Model 4 - Default Editor

Demo 5m:

When you make a commit, git may open an editor for you to write a commit message. The default editor is Vim. If you already know and love Vim, or would like to learn Vim, you can leave this. If you would prefer an editor that behaves like other editors you are used to, you should probably set it to something else. Assuming you have VS Code installed, you can set it to use it as follows

```bash
git config --global core.editor "code --wait"
```

### Model 5 - `git pull --ff-only`

5m

`git pull` tries to pull changes from another repository into your local
repository. This is safe when you don't have any new changes locally. Then
you are simply updating your local repository with those in the other.

But what if you have made changes in your local repository. And what if
someone has made new changes in the other repository. Effectively both
of you made changes "at the same time". Then when you try to pull, git
has to know how you want to deal with this situation.

We'll learn how to do this. But for now, we want to prevent git from automatically
taking any actions that would require us to possibly have to undo those
actions. In other words, if git detects such a situation, we want it to
stop, tell us there is a problem, and then we can decide how we want to
handle it. This makes `git pull` a safe operation. We configure git to do
this as follows:

```bash
git config --global pull.ff only
```

`ff` here stands for "fast-forward". Again you'll learn more about this later.


## Model 6

10m

Help your teammates to complete these above configuration on their computer.

## Summary

Git is highly configurable, but the good news is that the defaults are usually what you want. The configuration described above may be all you need to set. Again, to learn more about what configurations are possible, see `git help config` or <https://git-scm.com/docs/git-config> .

## Bonus

If you must work in a public lab and need to set these configurations regularly, you can create a script to automate the process. This is one of the strengths of the command-line interface, they are automate-able!

Create a plain-text file named `configure-git.bash` with the following contents with "Your Name" and "your.email@someservice.com" replaced with your information:

```bash
# File: configure-git.bash
git config --global user.name "Your Name"
git config --global user.email "your.email@someservice.com"
git config --global init.defaultbranch main
git config --global core.editor "code --wait"
```

Make this file executable:

```bash
chmod +x configure-git.bash
```

Now copy this file to a flash drive and carry it with you to the lab. When you sit down, in the lab, open Git-Bash and run the following, adjusting the drive letter appropriately.

```bash
bash /e/configure-git.bash
```

You could also create another script to cleanup before you leave.

```bash
# File: unconfigure-git.bash
git config --global user.name "Bogus name"
git config --global user.email "bogus.email@someservice.com"
```

Make it executable...

```bash
chmod +x unconfigure-git.bash
```

And copy it to your flash drive.


--------------------------
Copyright 2021, Darci Burdge and Stoney Jackson

This work is licensed under the Creative Commons Attribution-ShareAlike
4.0 International License. To view a copy of this license, visit
<http://creativecommons.org/licenses/by-sa/4.0/> .
