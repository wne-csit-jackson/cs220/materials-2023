## Markdown Activity

1. Switch roles. Have someone else be the recorder/pilot so they get a chance to manipulate GitLab.
2. Have your pilot/recorder create a new wiki page with the contents of the template below.
    * [Template (raw)](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/raw/main/Activities/Markdown-Activity-Template.md)
    * [Template (rendered)](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/blob/main/Activities/Markdown-Activity-Template.md)
3. Work through the activity, and save your answers.
4. Review answers as a class. Feel free to update and save your answers.
5. Edit this wiki page, and add a link to the wiki page your team just created.
   (***Expect explosions!***)
6. Have a class discussion about race conditions.

## Team Notes

* [Team](link)
