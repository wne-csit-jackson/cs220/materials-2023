# Part 4: Logical Conflicts

## Roles

* Manager
* Driver
* Recorder
* Skeptic


## Instructions


1. Download and extract git-conflicts-4.zip. Open the root of the project in
   VS Code. Open a terminal in VS Code which is also positioned in the root
   of the project. Inspect the repository as you did in Parts 1 and 3.
   Summarize your findings below.

2. Inspect the code in each branch. Do you think it would compile? If
   the classes and their methods were used, would they behave reasonably?

3. Merge non-master branch into `master`.

4. Was git able to merge the branches?

5. Inspect the merged code? Do you think it will compile?
   Do you think it will run reasonably?

6. From a developer point of view was the merge ***really*** successful?

7. Define a "logical merge conflict".

8. In a larger project, do you think you will easily be able to find such
   problems by inspection?

9.  How might help you be able to identify such problems?

10. Fill in the blank: After you merge you should always __________.

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
