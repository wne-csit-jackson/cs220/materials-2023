## Teams Activity

* [Template (rendered)](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/blob/main/Activities/Software-Setup-Activity-Template.md)
* [Template (raw)](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/raw/main/Activities/Software-Setup-Activity-Template.md)
Activities/Teams-Activity-Template.md)

## Team Notes

Do create a team page for this activity. In it record anything you find
important, useful, etc. If everything goes smoothly, it's OK to just create
the page and have no changes.

* [Team Name](link to wiki)
