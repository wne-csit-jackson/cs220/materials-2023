## Git Conflicts

### Part 1 - No Conflicts

* [Raw](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/raw/main/Activities/Git-Conflicts-1-Template.md)
* [Rendered](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/blob/main/Activities/Git-Conflicts-1-Template.md)
* [git-conflicts-1.zip](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/blob/main/Activities/git-conflicts-1.zip)

#### Team Notes

*

### Part 2 - Lexical Conflicts (Observe)

* [Raw](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/raw/main/Activities/Git-Conflicts-2-Template.md)
* [Rendered](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/blob/main/Activities/Git-Conflicts-2-Template.md)

#### Team Notes

*

### Part 3 - Lexical Conflicts (Practice)

* [Raw](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/raw/main/Activities/Git-Conflicts-3-Template.md)
* [Rendered](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/blob/main/Activities/Git-Conflicts-3-Template.md)
* [git-conflicts-3.zip](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/blob/main/Activities/git-conflicts-3.zip)

#### Team Notes

*

### Part 4 - Logical Conflicts

* [Raw](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/raw/main/Activities/Git-Conflicts-4-Template.md)
* [Rendered](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/blob/main/Activities/Git-Conflicts-4-Template.md)
* [git-conflicts-4.zip](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/blob/main/Activities/git-conflicts-4.zip)

#### Team Notes

*
