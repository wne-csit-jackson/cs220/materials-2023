## Software Setup Activity


### Create Accounts

* Create a [GitLab](https://gitlab.com/) account - You may have already done so.
  Good, but read on...
    * If you create one using 3rd party authentication (e.g., Google, Twitter, etc.), you'll need to set a password so that you can log in from the command line. To do this:
        1. Sign into GitLab.
        2. Open Preferences under user menu (upper right corner)
        3. Select Password in left menu.
* Create a [GitHub](https://github.com/) account - We'll need this for one of our activities (GitKit).

### Install Software

If you already have these installed, consider updating them to the most recent
versions and/or unstinstall and reinstall them.

I highly recommend installing all using default settings and on your main
hard drive (i.e., don't try to install them on a secondary drive or an external
drive).

* Install [Docker](https://www.docker.com/)
* Install [Git](https://git-scm.com/)
* Configure Git: within a terminal or git-bash
    ```bash
    git config --global user.name "YOUR COMMIT NAME"    # Use name that you don't mind becoming public.
    git config --global user.email YOUR@COMMIT.EMAIL	# Use email that you don't mind becoming public.
    git config --global pull.ff only                    # Makes pulls safe; but sometimes more annoying.
    git config --global core.editor "code --wait"       # Optional; makes VS Code your default commit message editor.
    ```
* Install [VS Code](https://code.visualstudio.com/)
    * Install VS Code extension: `Remote Development` (see [Extension Marketplace](https://code.visualstudio.com/docs/editor/extension-marketplace)). (Technically we only need `Dev Containers`. `Remote Development` includes it.)
