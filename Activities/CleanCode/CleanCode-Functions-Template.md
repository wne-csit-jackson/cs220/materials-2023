# Clean Code - Functions

Watch the Functions video.

Start: 9:20
End: 55:03

Total time: 47m

As you watch, answer the questions below as you watch. If a question
requires some thought, come back to it after the video is done.

## First rule of functions

1. What is the "First rule of functions"?

2. What does Uncle bob think about these lengths for functions?

    * 4 lines
    * 5 lines
    * 6 lines
    * 10 lines

3. If you follow this rule and you have a `try` or an `if` or a `loop`, what
    must the body of the `try`, `if`, or `loop` be?

    Example:
    ```
    very_descriptive_function_name() {
        if (some_condition()) {
            // What kind of statement is this likely to be?
        } else {
            // Same question.
        }
    }
    ```

4. How does keeping functions small improve the readability of your code?


5. Uncle Bob uses Eclipse for Java development, which has a bunch of cool
    refactoring tools. Does VS Code have refactoring tools for Java too?
    Paste a link that supports your answer.

## Are You Out of Your Mind

6. What keeps you oriented in code written as Uncle Bob suggests?

## The Geography Metaphor

## The Bedroom Metaphor

7. Why is it important for a team to organize code as Uncle Bob suggests?

## Efficiency

## Coding Time

8. What argument of Uncle Bob's did you find most convincing?

9. What argument of Uncle Bob's did you find least convincing?

## Where do classes go to hide?

10. According to Uncle Bob, what is a long function, ***really***?

11. What did Uncle Bob create before he started refactoring the example code? Why?

12. What is Uncle Bob's first step in refactoring a long function?

13. Second step?

## One Thing!

12. Anyone know the movie referenced by "One Thing" ?

13. What are two symptoms that a function does more than one thing?

## Extract Till You Drop!

14. How do you ensure that a function does one thing?

15. What is "feature envy"? Feel free to search for it.

## Discuss and reconcile your answers with your team

## What questions to you have?

Prepare a couple questions for the class and your instructor.
Remember, we don't get out of here until 3 questions are asked!

## Homework prep

* Identify functions in your code that are probably too long.
* Following Uncle Bob's example, form a plan for refactoring them.
