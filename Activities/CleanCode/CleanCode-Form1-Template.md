# Clean Code - Form 1

* Start: 00:11:10
* End: 00:45:50
* Total: 35m

## Coding Standards

1. Should a coding standard exist?

2. What should be the coding standard?

3. What do we when there are lots of comments?

## Comments Should be Rare

4. What color should comments be?

5. What's the most reliable documentation of code?

(Discussion/Activity: Set your IDE's color for comments to be bright red!)

## Comments are Failures

6. What does Uncle Bob mean by this?

7. When do you need comments?

8. When don't you need comments?

## Comments are Lies

9. How do comments become lies?

## Good Comments

10. List them.

## Bad Comments

11. List them.

(Discussion: What is the DRY principle?)

## Explanatory Structure

12. How do functions help?

## Formatting

## File Size

13. Is file size and project size correlated?

14. What's the typical...

    * largest file size?
    * smallest file size?
    * average file size?

## Vertical Formatting

15. How does "small functions" interact with "blank lines"?

16. What should be close together?

## Horizontal Formatting

17. What's a typical....
    * shortest line
    * longest line
    * typical line

## Indentation

18. Most important issue here?

19. What's Uncle Bob's preferences?

## Discussion and Homework

* Reconcile your answers with your team.
* Discuss the "discussion" questions above.
* Review your code for your homework with respect to this section.
