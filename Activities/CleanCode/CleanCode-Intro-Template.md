# Intro to Clean Code

Watch 1h video: Clean Code (remake) by Robert Martin.

* Start: 7m 28s
* Runtime: 47m

1. What killed Sword Inc. according to Uncle Bob's paraphrase of his
   interview with a former employee?

2. The productivity trap

    a. What happens when we add more programmers to an existing project?

    b. Who performs the redesign?

    c. What is the productivity trap?

3. What is code rot?

4. What is a rigid system?

5. What is a fragile system?

6. What is an inseparable system?

7. What is opacity in software?

8. Why does code rot?

9. What's the only way to go fast?

---

After the video...

1. Review your answers above as a team, and try to reach a consensus answer.


2. What are the major takeaways from this video and/or what important
   details do you want to remember?


3. What questions does your team still have?


---

PRE-HOMEWORK

In your homework for Clean Code you'll be fixing up some code that you
have previously written. So, as a first step, identify some code that
could use a clean up. It's important to pick something that you are
familiar with (you know roughly what it does and how it works). And it
needs improvement (in my experience, most code needs improvement). Here
are some other constraints.

- It doesn't have to be pretty. In fact, it's better if it is a bit rough.
- Ideally it runs and "works", but may have bugs.
- The language it's written in doesn't matter; as long as it is a standard
   high-level, 3rd generation programming language --- for example: Java, C,
   C++, Python, Javascript, etc.
- Is more than 2 files.
- One of the files is more than 50 lines.
- Preferably code that you wrote. If you didn't write it, each file must have
   the author(s) in comments at the top and you need to understand what it
   does and roughly how it works.

Once you've identified some code. Copy it into a subdirectory within
your course folder on your local machine, and place it under Git version
control.

Talk to your instructor if you need help identifying something to improve.
