# Clean Code - Names++

Watch Names++

* Start: 6:25
* Stop: 41:56
* Time: 35

## Intent-revealing names

1. What symptom of a bad name does Uncle Bob highlight in this section?

2. What should be done with constants/literals (e.g., 1, 3.14, etc.)?

## Describe the problem

1. What symptom of a bad name does Uncle Bob highlight in this section?

2. To solve the poor name, Uncle Bob made use of established terms in the problem domain.

    * What problem domain did he borrow from?

    * What is a problem domain?

## Avoid disinformation

1. What is meant by disinformation in a name?

2. What symptom of a bad name does Uncle Bob highlight in this section? How is it different from the one in "Intent-revealing names"?

## Pronounceable Names

1. Give an example of an unpronounceable name.

2. Rewrite your example so it is pronounceable.

## Avoid Encodings

1. Why don't we need to encode types in variable names anymore?

2. Uncle Bob tells us to "Avoid Encodings". What do you think Uncle Bob's opinion is about private and protected members in Python which are prefixed with `_` and `__` respectively?

3. How

## Parts of Speech

1. Classes should be __________ phrases?

2. Methods and functions should be ________ phrases?

3. What are examples of "noise" words?

4. What should begin with "is"?

5. What part of speech are enum values likely to be?

6. Why is using appropriate parts of speech important?

## The scope length rule

1. Summarize the scope length rule for ***variables***.

2. Summarize the scope length rule for ***methods/functions/classes***.

3. Summarize the rule with respect to visibility (private/public)

## Discuss

After the video...

Discuss your answers as a team and create a single team answer for the above
and post them as usual.

## What questions do you have?

Identify and list questions that your team wants to raise during the class
discussion.


## Homework (Cont'd)

This section will help you with your CleanCode homework.

1. Review the names in your code. List those that need improvement.
   In terms of Clean Code why do these names need improvement?
   Discuss them with your team mates and get their opinions.

3. Select some better names, and discuss those with your team as well.
