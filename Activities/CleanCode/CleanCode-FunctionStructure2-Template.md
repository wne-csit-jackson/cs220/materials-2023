# Clean Code - Function Structure (2)

## Command Query Separation

1. Define.

(Discussion: Can you find an example of a violation or compliance in your code?)

## Tell Don't Ask

2. Define.

(Discussion: What's the advantage? Can you find an example of a violation or compliance in your code?)

3. Law of Demeter. Define. (Instructor will pause the video.)

(Discussion: What's the advantage? Can you find an example of a violation or compliance in your code?)

## Structured Programming

4. The 3 basic operations?

5. Self-similar structure is based on what characteristic?

(Discussion: Why is this important?)

## Early Returns

How does Uncle Bob feel about...

6. Returns in an if (outside any loop)?

7. Returns in loops?

8. Continues in loops?

9. Breaks in loops?

> "It is more important for you to make your code understandable than it is for you to make your code work." - Uncle Bob

10. What if the loop is small?

11. If you keep your functions small ... ?

## Error Handling

(Enjoy the Stack TDD Kata. A Kata is an exercise for training to practice a form. TDD is Test Driven Development. We'll be learning more about this later.)

## Errors First

## Prefer Exceptions

12. Should we return error values, or raise exceptions?

## Exceptions are for Callers

13. What type of exceptions should we throw?

(Discussion: What is a "scoped exception"?)

## Use Unchecked Exceptions

(Discussion: why not use checked exceptions?)

14. What does Uncle Bob say about passing a message to an exception?

## Special Cases

## Null is not for Error

(Discussion: this is a special case of with rule above?)

## Null is a Value

(Discussion: Is Null being used as an error value?)

> Returning Null to mean "not found" only works if Null is never allowed as a value in a collection.

> "I call [the null reference] my billion-dollar mistake" - Tony Hoare, Turing Award winner and inventor of the null reference. https://en.wikipedia.org/wiki/Tony_Hoare#Apologies_and_retractions

15. A function that performs error handling ... ?

## Conclusions

(We will skip because it's hard to understand.)

## Discussion and Homework

* Reconcile your answers with your team.
* Discuss the "discussion" questions above.
* Review your code for your homework with respect to this section.

