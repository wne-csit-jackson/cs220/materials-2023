# Clean Code - TDD2

* Start: 00:03:04
* End: 01:12:27
* Time: about 65m

## Red Green Refactor

1. Jot down any questions you have about the rules of bowling.

(Discussion: try to answer all of your teams' questions.)

2. What does Uncle Bob do with the design after creating it?

(Discussion: How detailed should a design be? What good are designs?)

3. During refactoring, list types of problems that Uncle Bob commonly fixes (keep updating this answer).

4. What does Uncle Bob do when his design doesn't allow him to easily pass a test?

5. Should tests be refactored/cleaned?

6. What kind of test does pick for the "next test" to implement?

## Answer the Objections

1. Does TDD slow you down? (Discussion: why?)

2. Should you get your boss's permission to do TDD? (Discussion: why?)

3. Why not write code correct the first time?

4. Who or what tests the tests?

5. Can tests prove the code is correct? Is this the goal of tests?

6. Is it OK to write tests after code? (Discussion: Why?)

7. What book will help you working with legacy code that doesn't have tests?

8. How do you test GUIs?

9. How do you test DBs?

10. Should programmers write tests, even if it is hard to do?

## Discipline and Professionalism

## Double Entry Bookkeeping

## QA Should find NOTHING

## 100% Code Coverage

11. Is it possible to reach 100% coverage? (Discussion: can you think of reasons you can't?)

## Doctors wash your hands!

## Conclusion (we'll skip for the sake of time; you might want to watch it when reviewing)

## Discussion (skip until the video is over)

1. Uncle Bob is using JUnit 4. You'll be using JUnit 5. Find links for the following:
    * JUnit 4 documentation:
    * JUnit 5 documentation:
2. Migrating from JUnit 4 to JUnit 5:
    * Uncle Bob used @Before. JUnit 5 does not have @Before. What should you use instead?
    * How about @Ignore?
    * How about testing for exceptions?
