# TDD Practice

## Goal

Practice TDD.

## Objective

Use TDD to build a program that determines which of two poker hands wins, or if there is a tie.

## Poker Overview

Poker is a game played with a standard 52-card deck of playing cards [1]. Some variants use jokers; we'll ignore jokers. There are many variants of poker, but all come down to determining which 5-card poker hand each player has, and then determining who's hand is the highest ranked hand [2]. Note that players my have two hands of the same kind, but one hand may still be ranked higher than another based on the rules for each kind of hand (see [2]). It is still possible to tie.

- [1] 52-card deck: <https://en.wikipedia.org/wiki/Standard_52-card_deck>
- [2] List of poker hands: <https://en.wikipedia.org/wiki/List_of_poker_hands>

## Quick Design

![design](poker-design.png)

Observations:

* A poker hand has 5 cards.
* There are several different types of poker hands (inheritance/subtypes).
* The type of a poker hand is determined by the ranks and suits of the cards in the hand.
* Each card has a suit and a rank.

So let's start with a Hand class that has a static method that constructs
a Hand from 5 cards, and has an instance method that determines if the current
Hand beats another. Then a tie is simply the situation when neither hand beats
the other.

## Getting Started

Your instructor will provide a TDD starter project and instructions for using it.

## Tips

### To-do list

Keep a list of test ideas and other creative ideas that you want to delay,
but don't want to forget. Update it regularly.

### Git

Make a commit after each major action. Here is an example log.

```bash
revert: pass: hand with highest card wins
pass: hand with highest card wins
redesign: hand with highest card wins
fail: hand with highest card wins
clean: throw exception when creating a hand with other than 5 cards
pass: throw exception when creating a hand with other than 5 cards
fail: throw exception when creating a hand with other than 5 cards
clean: can create a hand
pass: can create a hand
fail: can create a hand
```

Use a prefix that indicates which stage you just completed, and the rest
of the message to identify the test/functionality you are trying to implement.
Here is a summary of the prefixes you see above.

* fail: You wrote just enough of a test to fail. One test fails.
* pass: You wrote just enough code to pass the failing test. All tests pass.
* clean: You refactored to clean your code and tests. All tests pass.
* redesign: All tests pass (the previously failing test is @Disabled)
* revert: Revert a commit (usually the last one) because you made a mistake
    and want to try again.

Doing this will give you a safety net, and help you apply TDD intentionally.
