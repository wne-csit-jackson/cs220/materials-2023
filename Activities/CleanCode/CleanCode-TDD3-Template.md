# CleanCode TDD Part 3

Work in pairs or triples.

## Roles:

* Recorder:
    * Take notes (this will be VERY important here)
* Pilot:
    * Perform the steps for TDD.
* Copilot (distribute this role in a pair):
    * Guide the team with slides.

## Goals

* Setup a new project using instructor provided template.
* Refactor code using VS Code extensions for Java.
* Write unit tests in JUnit 5.
* Delay work during TDD using a todo list.
* Reproduce Uncle Bob's Bowling Game Kata to practice TDD.

## Setup project

Your instructor will demonstrate how to create a project for this
exercise.

1. Take notes and ask questions.

2. Replicate steps. Note any tips that may help you later.

## The Bowling Game Kata

* Website: http://butunclebob.com/ArticleS.UncleBob.TheBowlingGameKata
  * First link contains slides.
* Slides: https://www.slideshare.net/lalitkale/bowling-game-kata-by-robert-c-martin

Review the rules of bowling and the initial design.

## Begin - BowlingGameTest

1. Take notes and ask questions.

2. Replicate steps. Note any tips that may help you later.

## The first test - testGutterGame

1. Take notes and ask questions.

2. Replicate steps. Note any tips that may help you later.

## The second test - testAllOnes

1. Take notes and ask questions.

2. Replicate steps. Note any tips that may help you later.

## The third test - testOneSpare

1. Take notes and ask questions.

(Don't comment, but use JUnit 5's @Disable annotation)

2. Replicate steps. Note any tips that may help you later.

## The fourth test - testOneStrike

1. Replicate steps. Note any tips that may help you later.

## The fifth test - testPerfectGame

1. Replicate steps. Note any tips that may help you later.

## The secret sixth test - testNegativeThrowsNegativeRollException

1. Take notes and ask questions.

2. Replicate steps. Note any tips that may help you later.

## The secret seventh test - testRollMoreThan10InFrameThrowsTooManyPinsException

1. Replicate steps. Note any tips that may help you later.

## Change Pilots and Repeat


## Try at Home

Delete your fork and try again.
