# Clean Code - Function Structure (1)

* Start: 9:03
* End: 47:26 (Just before CQS)
* Elapsed: 38m

## Arguments

1. Are arguments a liability or an asset?

(For discussion) Why?


## Three Argument Max

2. What stratedy is given to reduce the number of arguments.

(For discussion) Do you think this is the only or first strategy?
What is Uncle Bob's intent with suggesting this strategy?

## No Boolean Arguments Ever

3. What's the problem?

4. What's the fix?

## Innies not Outties

5. Which is prefered?

6. Why is the other not preferred?

7. What's the solution?

## The Null Defense

8. What's the problem?

9. What's the fix?

10. What is defensive programming?

11. Where is defensive programming good?

## The Step-Down Rule

12. Summarize the step-down rule.

## Switches and Classes

13. What's wrong with switch statements?

14. What can we do to deal with switch statements (there are two strategies)?

15. Python doesn't have switch statements. So it does not suffer from this
    problem. Right? WRONG! Why?

## Paradigms

16. List them.

## Functional programming

17. Essence?

## Side Effects

18. Define

19. What is temporal coupling?

20. How do you fix a temporal coupling?

## Discussion

Discuss your answers with your team members and discuss those
questions marked "(for discussion)".

## Homework continued

Review your code. What issues in this section applies to your code.
Discuss them with your team and see if they agree. Discuss you
you might fix it.
