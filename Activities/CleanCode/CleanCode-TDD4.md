## CleanCode TDD4

* [Rendered](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/blob/main/Activities/CleanCode/CleanCode-TDD4-Template.md)

To get started:

1. Fork https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class-ro/bowlinggame , and name your fork "poker".
2. Clone your fork.
3. With Docker running, open the root of your clone in VS Code and allow it to
    reopen in a devcontainer.

Enjoy!
