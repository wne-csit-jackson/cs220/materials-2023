# Clean Code - TDD 1

* Start at 00:10:42
* End at 01:01:47
* About 50m

## Fear and Code Rot

1. What prevents us from cleaning our code?

## The Big Clean Up

## Eliminating Fear

2. Under what condition do they ship Fitness?

## Demonstration

3. What let's you clean your code without fear?

## The Real World

## The Three Laws of TDD

4. What is TDD?

5. What are the three laws of TDD?

6. How long is a typical test-code cycle?

## Debugging Time

(Discussion: How does TDD reduce debugging time?)

## Design Documents

7. What serves as a low-level design document?

## Decoupling

8. What makes production code testable?

9. What makes your code decoupled?

## Courage to Change

10. What eliminates the fear to clean up your code?

## Trust

11. How do you get a suite of tests that you trust?

(Discussion: why are tests written after you write code likely
worse than those written before you write code?)

## Conclusion

(Note anything you missed or would like more clarification about.)

## Discussion and Homework

* Reconcile your answers with your team.
* Discuss the "discussion" questions above.
* Review your code for your homework with respect to this section.
