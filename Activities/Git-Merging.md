## `git init`

* [Raw](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/raw/main/Activities/Git-Merging-Template.md)
* [Rendered](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/blob/main/Activities/Git-Merging-Template.md)

## Example Repository

The following zip contains a project. Have your driver download it and
extract it. You'll be working on it during the activity.

> **IMPORTANT**
>
> On Windows, be sure to right click on the zip file and extract it.
> ***Do not*** simply double-click it. That only allows you to browse its
> contents, but does not extract its contents.

* [git-merging-1.zip](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/raw/main/Activities/git-merging-1.zip)
* [git-merging-2.zip](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/raw/main/Activities/git-merging-2.zip)
* [git-merging-3.zip](https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/home/-/raw/main/Activities/git-merging-3.zip)

## Team Notes

*
