# Linux Shell Commands - Activity

The command-line allows us to execute commands using a text-based
interface. We'll be using the command-line to navigate file-systems,
manipulate files and directories, and interacting with git.

This activity introduces some basic Linux commands for navigating a
file-system and manipulate its files and directories. The commands
presented in this activity should work in most Linux shells. Many of
them will also work in PowerShell for Windows. For the purposes of this
activity, we'll assume a Bash-like shell.

> **Why Bash? Why Linux? Why open-source?**
>
> Open-source is everywhere. Linux is an open-source operating system,
> which has long been used to run most servers and is now the foundation
> for many personal devices (e.g., Android), set-top boxes (e.g., TiVo),
> and many smart devices (e.g., Ecobee), and serves as the foundation
> of most cloud-based computing platforms (e.g., AWS and Google Cloud).
> Big companies like Microsoft and Google are increasingly using and
> contributing to open-source software, recognizing its importance to
> their business model (e.g., Microsoft purchased GitHub, VS Code is
> made by Microsoft and it's open-source, IBM purchased RedHat, etc.).
> So, as a software developer, you need to be familiar with open-source
> and its ecosystem of tools. That means Linux, Linux-based shells,
> Linux commands, etc. Also, git itself is an open-source version control
> tool invented by the same person who invented Linux, Linus Torvalds.

## Roles

* Manager: INITIALS
* Driver: INITIALS
* Recorder: INITIALS
* Spokesperson: INITIALS


## Model 1 - Terminology

Lecture: 5m

A **terminal** is a program that runs a **shell**. A terminal used to be a physical machine, but now it is a program like Terminal.app, iTerm, etc. Basically, the terminal allows you to type text with the keyboard and view text on your screen.

A **shell** is a program that interprets and executes command-line commands: e.g., Bash, Zsh, DOS, and PowerShell. These shells are essentially programming languages that specialize in manipulating an operating system and its filesystem. DOS, for example, stands for Disk Operating System. Each command you type into a shell is interpreted and executed, and its results are printed to the terminal.

In some cases these terms are used interchangeably.

## Model 2 - Starting a Linux-based Shell

Lecture/Demo: 10m

In this class, we'll be using a Linux-based shell like Bash or Zsh. Linux-based shells share a common set of commands and syntax.

On **Windows**, you can get a Linux-based shell by installing Git for Windows, which gives you Git-Bash. As its name suggests, Git-Bash runs Bash. It also provide a set of common Linux commands through the MinGW project. If you haven't installed Git for Windows, please do so now.

**Mac** comes with Terminal.app, which is your default terminal application, and Zsh, which is your default shell. You can also install iTerm.app, which is another popular terminal for Mac.

**On either OS**, VS Code includes a builtin terminal which makes it easy to access the command-line without having to run a separate application. This is the approach we recommend as it will make it easier to work with the command-line while you are working on a project. You start your terminal by pressing CTRL + ` .

**On Mac**, VS Code will run your default shell, Zsh; so you are all set.

**On Windows**, VS Code will run DOS or PowerShell as your default shell. These are not Linux-based shells. So you'll need to change your default shell to Git-Bash. Here's how.

1. Start VS Code.
2. Start a terminal by pressing CTRL + `
3. On Windows, if your shell is not git-bash, set the default shell to Git-Bash by ...
   1. Selecting `select default shell` from the the terminal dropdown box (see figure below) and choose git-bash
      ![Terminal Dropdown Box](terminal-dropdown-box.png)
   2. Then exit the terminal by typing `exit` in the shell.

> :boom: Problem? Don't see git-bash in the dropdown? First, make sure you have git-bash installed. Second, if when you installed git-bash you installed it to a non-default location, then you have to manually tell VS Code where to find it. It might be easier to uninstall git and reinstall it into the default location. Or you can learn how to configure VS Code to point it to the new location. Here's a link to get you started <https://code.visualstudio.com/docs/editor/integrated-terminal#_configuration>

Now when you start a terminal in VS Code, it will run Git-Bash.

It's also handy to know how to start a terminal in a particular directory.

* For Git-Bash, you can right-click in a your file browser and select "open git-bash here", and it will open in the same directory that the file browser is open to.
* For VS Code on Windows, when you installed it, if you selected `Add "Open with Code" action...`, then you can right-click a folder in Windows Explorer and selection `Open with Code` and it will open in the same folder. Then when you start a terminal ( CTRL + ` ) it will also start in that same directory.
* For VS Code on MacOS, add VS Code to your Dock. Then drag folders to it to open VS Code in that folder.

### Instructions

Time: 10m

1. Help each of your team members figure out how to start VS Code start a terminal in a folder from your file browser, and then open a terminal inside VS Code running Git-Bash or Zsh terminal.

## Model 3 - Linux Commands

Intro: 5m

>:information_source: Square brackets indicate optional arguments.

* `pwd`
  * Print the absolute path to the current working directory.
* `ls [-a]`
  * Display the contents of the current working directory.
  * `-a` displays all files, including hidden files.
* `cd` *[path]*
  * Change the current working directory to the provided path. If no path is provided, change to the current user's home directory. If the path is absolute, change to that directory. If the path is relative, change to that directory relative to the current directory.
* `mkdir` [`-p`] *path*
  * Create the new directory given at the end of path. If `-p` is provided, creates all directories given in the path.
* `touch` *path*
  * Update the last-modified time of the file or directory named by path, or create a new file if the named file doesn't exist.
* `rm` [`-r`] *path*
  * Remove the file named by path. `-r` allows you to delete a directory and all its contents recursively.
>:warning: Be careful with `rm` and `rm -r`. There is no "trash". If you delete something, it's gone.
* `cat` *path* [...]
  * Display the contents of each file.
* `less` *path*
  * Display the contents of a file using a pager. Press `b` to go back a page, `spacebar` to go forward a page, `/pattern` to search for pattern and `n` to find the next occurrence, and `q` to quit.
  * This pager is used in some `git` commands and `man`.
* `man` *command*
  * View the documentation for *command*.
  * Uses the `less` pager.
* `info` *command*
  * Use this if `man` doesn't return anything useful because the command is a builtin command.

> :boom: Problem? Is `man` and `info` not working in Git-Bash? That's because Git-Bash does not ship with these commands. Instead, you'll need to use each commands builtin help (e.g., `git help`), or use Google and search `man git`.

### Instructions

Time: 10m

1. Have your driver try each of the linux commands above.
2. Exit your terminal by typing `exit` into the shell.

> :boom: Problem? On MacOS, if you run into "Operation not permitted", check out the following article: <https://osxdaily.com/2018/10/09/fix-operation-not-permitted-terminal-error-macos/>

## Model 4 - Open File Browser or VS Code from Command-Line

Demo: 5m

Sometimes it is handy to launch a file browser or VS Code from the command-line and be located in the same directory as your command-line.

MacOS has the `open` command, which runs the default application associated with the file you pass it. So when you run `open .`, since `.` is a directory (the current working directory), MacOS will start a Finder opened to the current working directory. The example below will locate your command-line and file browser in the same directory.

```bash
cd some/directory
open .
```

Windows has the `start` command, which runs the default application associated with the file you pass it. So when you run `start .`, since `.` is a directory (the current working directory), Windows will start a File Explorer opened to the current working directory.

 The example below will locate your command-line and file browser in the same directory (using Git-Bash).

```bash
cd some/directory
start .
```

On any OS, when you installed VS Code, it may have also installed a command line command named `code` to launch VS Code in a given directory.

```bash
cd some/directory
code .
```

### Instructions

Time: 5m

1. Try to navigating to a directory in a terminal and launching file browser and VS Code in the same directory.

## Model 5 - Tab Completion and the History

Demo: 5m

It's hard to remember the details of every command and it is easy to mistype a command. Using a shell's tab-completion and a history features can help.

* Tab-completions - Start to type the name of a command, directory, file, etc., and then press tab. If what you typed is enough to uniquely identify the thing you are trying to type, the shell will complete it for you. If not, you'll probably get some beep. Press tab again and the shell may show some possible completions and allow you to select from them. This is handy to avoid typos and helps you remember the exact name of something.
* History - use the up and down arrows to traverse the history of commands you've recently typed. When you find the one you are looking for, press enter to execute it again, or edit it first and then press enter.
* History-completion - I made up this term. But some shells will let you type a little on the command-line, and then press the up arrow to search through your history filtering by what you just typed. For example, if you know you are looking for a recent `git` command -- type `git` then use the up and down arrows to walk through your history of `git` commands.

### Instructions

Give them a shot.

Time: 5m

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2021, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
