# Branches

## 1. Roles

Work in pairs or triples. Assign each role to one of your team members.

* Driver:
* Reader:
* Recorder:
* Spokesperson:

Working together

* Have the manager track time, ensure participation, and help the team
  work effectively together.
* Have the reader read **out loud** each section and question.
* Have the driver carry out the commands in each section.
* Have the recorder note new commands, options, concepts, insights, and
  questions the team has at the end of each section.

## 2. Overview

5m

A branch represents a line of development within a repository. It's common for projects to use the default branch, `main`, as the "stable" branch. This means that at any given time, if someone checks out the `main` line, it should compile and run reasonably well. But that means, we should not add commits to `main` unless we are sure they work. So when we start working on a new feature or fixing some bug, the first thing we do is start a new branch, and work on that branch until what we have is ready for inclusion in `main`.

Let's look at how branching works.

1. What questions your team has at this point?

2. List the important concepts and key insights?

## 3. Create a new local repository

5m

Let's create a project that we can experiment with.

```bash
git init test-branching
cd test-branching
touch 1
git add .
git commit -m "1"
git log --oneline
```

Notice `--oneline` causes `git log` to print each commit on a single line. And we see something like the following

```
c56859a (HEAD -> main) 1
```

Here we see that `HEAD` points to `main`, and `main` points to the first commit we made.

The commit `HEAD` points to (transitively) is the commit that is currently checked out in our working tree.

1. What questions your team has at this point?

2. List the important concepts and key insights?


## 4. HEAD, the active branch, and commits

5m

Also, the branch `HEAD` points to is called the ***active branch***. So here, `main` is the active branch. The active branch will be updated when new commits are made. Let's try that now.

```bash
touch 2
git add .
git commit -m "2"
git log --oneline
```

Now our commit history looks something like this.

```
549f908 (HEAD -> main) 2
c56859a 1
```

`HEAD` points to `main` which points to our second commit. So both `HEAD` and `main` are advanced when we make a new commit. Let's do it again.

```bash
touch 3
git add .
git commit -m "3"
git log --oneline
```

And here's our log

```bash
c6c9ff2 (HEAD -> main) 3
549f908 2
c56859a 1
```

1. What questions your team has at this point?

2. List the important concepts and key insights?


## 5. Create a branch

10m

Now let's create our first branch.

```bash
git branch new
git log --oneline
```

Which yields

```bash
c6c9ff2 (HEAD -> main, new) 3
549f908 2
c56859a 1
```

We now have a new branch named `new`. It also points to the last commit we made, because that's where `HEAD` was when we created it.

Let's add another commit and see what happens (can you predict what is about to happen?).

```bash
touch 4
git add .
git commit -m "4"
git log --oneline
```

Yields

```bash
3d7b38b (HEAD -> main) 4
c6c9ff2 (new) 3
549f908 2
c56859a 1
```

Notice `HEAD` and `main` advanced to the fourth commit, but `new` is left behind on the third. That's because HEAD always advances to the new commit, and the branch that HEAD points to, the ***active branch***, also advances to the new commit. And that's it. No other branches advance.

Let's make another commit.

```bash
touch 5
git add .
git commit -m "5"
git log --oneline
```

Yields

```bash
85ea445 (HEAD -> main) 5
3d7b38b 4
c6c9ff2 (new) 3
549f908 2
c56859a 1
```

At this point, this probably didn't surprise you.

1. What questions your team has at this point?

2. List the important concepts and key insights?


---

WAIT FOR CLASS DISCUSSION

---


## 6. Switch branches

10m

So, now let's switch and start working on our new branch.

```bash
git switch new
git log --oneline
```

Yields

```bash
c6c9ff2 (HEAD -> new) 3
549f908 2
c56859a 1
```

OK, we see `HEAD` now points to `new` which points to the third commit.

This also means that the contents of our working tree is back to what it was on the third commit. You just time traveled!

But wait! What happened to `main`? Don't worry, it's still their. By default, `git log` just shows us the linear history from the current commit back to the first commit. If other branches are not on that path, you will not see it. Let's add `--all` to `git log` show all branches.

```bash
git log --oneline --all
```

Yields

```bash
85ea445 (main) 5
3d7b38b 4
c6c9ff2 (HEAD -> new) 3
549f908 2
c56859a 1
```

I feel better now.

1. What questions your team has at this point?

2. List the important concepts and key insights?


## 7. Checkout branches

5m

`git switch` is a newer command, which mostly does the same thing as an older command, `git checkout`. Git is slowing evolving its interface. So `git switch` is the new preferred command to switch branches, but you should know `git checkout` too because lots of documentation out there still refers to it. Let's use `git checkout` to get back to `main`.

```bash
git checkout main
git log --oneline --all
```

And then use either `switch` or `checkout` to get back to `new`.

```bash
git switch new
git log --oneline --all
```

1. What questions your team has at this point?

2. List the important concepts and key insights?


## 8. Improve log

10m

Now let's create a new commit. Try to predict what the commit log will look like before you do it.

```bash
touch 6
git add .
git commit -m "6"
git log --oneline --all
```

Let's see if you were right...

```bash
05718a8 (HEAD -> new) 6
85ea445 (main) 5
3d7b38b 4
c6c9ff2 3
549f908 2
c56859a 1
```

Hmm... Yes, HEAD points to new which points to the sixth commit, but I'm surprised to see the fifth commit below it, because the 6th did not build on top of 5th. It build on top of 3rd. Remove `--all` from `git log` and see what happens.

```bash
git log --oneline
```

```bash
05718a8 (HEAD -> new) 6
c6c9ff2 3
549f908 2
c56859a 1
```

That's more like what I expected. But I still want to see the other branch, but I want it to more accurately show which commits belong to which branches. So let's see if we can improve `git log` so that it's results aren't so deceptive. Try adding `--graph`.

```bash
git log --oneline --all --graph
```

```
* 05718a8 (HEAD -> new) 6
| * 85ea445 (main) 5
| * 3d7b38b 4
|/
* c6c9ff2 3
* 549f908 2
* c56859a 1
```

That tells a much better story. Now we can see that `new` and `main` share the first three commits, but then their histories diverge.

1. What questions your team has at this point?

2. List the important concepts and key insights?


## 9. Create a branch and switch to it in one command

5m

One more trick. Usually when you create a branch you also want to start working on it immediately. So Git provides a short hand. You can provide `-c` to `switch` or `-b` to `checkout` and Git will both create and switch to the named branch. Let's try it.

```bash
git switch -c another
git log --oneline --all --graph
```

```bash
* 05718a8 (HEAD -> another, new) 6
| * 85ea445 (main) 5
| * 3d7b38b 4
|/
* c6c9ff2 3
* 549f908 2
* c56859a 1
```

1. What questions your team has at this point?

2. List the important concepts and key insights?



## 10. Configure an alias

5m

`git log --oneline --all --graph` is rather long to remember and type. In Git, you can create aliases, which are like custom commands that expand into the real command. Aliases are stored in your Git configuration. So we use `git config` to modify them.

Let's add a `graph` alias to our global config.

```bash
git config --global alias.graph 'log --oneline --all --graph'
```

Now you can use `git graph` to view the graph of your commit history!

1. What questions your team has at this point?

2. List the important concepts and key insights?


> TIP: Here is a more complicated `git graph` alias which also shows each
> commit's author and the time the commit was made.
>
> `git config --global alias.graph "log --all --decorate --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr, %cn)%Creset' --abbrev-commit --date=relative`"


## 11. Practice

15m

Have each of your team members try creating and switching to different
branches, and making commits to different branches. Use your new-found
log powers to display the graph as you go.


---
Copyright 2021, Stoney Jackson <dr.stoney@gmail.com>
Licensed under CC-BY-SA 4.0 International.
