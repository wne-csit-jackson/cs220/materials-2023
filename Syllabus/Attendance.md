## Attendance

Attendance is required and important. Roll will be occassionally recorded,
but will not directly count towards your grade. Your attendance record for
this class is available in Self-Service. When absent, you are responsible
for all material presented in course.

I use an active learning pedagogical approach in which most learning takes
place in the classroom. Thus, there is typically a positive correlation between
attendance and grades.
