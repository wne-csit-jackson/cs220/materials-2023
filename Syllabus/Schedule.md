## Rough, Tentative Schedule

Below is a rough schedule of topics. A more detailed, up-to-date
schedule will be kept along with our class notes. Please follow along there.

* Week 1 - Licensing
* Week 2 - Agile and Scrum
* Week 3 - Command line
* Week 4 - Git
* Week 5 - Git
* Week 6 - Git Workflow
* Week 7 - Git Workflow
* Week 8 - Clean Code
* Week 9 - Clean Code
* Week 10 - Clean Code
* Week 11 - Testing
* Week 12 - Testing
* Week 13 -  Testing
* Week 14 - Docker
* Week 15 - Docker
