## Instructor Information

- Stoney Jackson, PhD
- Email: [Stoney.Jackson@wne.edu](mailto:Stoney.Jackson@wne.edu)
- Appointments: [https://stoneyjackson.youcanbook.me/](https://stoneyjackson.youcanbook.me/)
- Office: H201b and class Discord server
- Class Discord invite link: https://discord.gg/QgQ24BR9 (expires Sunday 1/22/2023)

### Office Hours

- 9-10:30 AM ET, Mon, Tue, Wed, and Thu.

During office hours, I am available in our class Discord server and
in my office. You do not need an appointment to meet with me during
these hours. In-person takes priority.

### Outside Office Hours

You may also contact me outside office hours on Discord, or by email.
Typically I check email and Discord in the morning when I first get to
the office. Please allow up to 1 business day for a response.

### Appointments

To meet with me outside office hours, make an appointment using the link below.

- https://stoneyjackson.youcanbook.me/
