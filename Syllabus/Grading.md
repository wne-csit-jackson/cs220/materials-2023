## Assessment

- 45% Exams (3 @ 15% each)
- 55% Homework (Between 5-7 assignemtns; 1 every 2-3 weeks)

### Grading Scale

The following scale is used to map overall scores to letter grades. The top is
the minimum overall percentage needed to earn the letter grade below. Overall
scores are rounded to the nearest percent before converting to a letter grade.

[%header,format=csv]
|===
0, 60, 67, 70, 73, 77, 80, 83, 87, 90, 93
F, D, D+, C-, C, C+, B-, B, B+, A-, A
|===


### Exams

Exams assess your knowledge of concepts, terminology, and their correct
application. Each exam primarily covers the material since the last exam.


### Homework

Individually, you will complete assignments. These will be used to deepen
your knowledge and assess your ability to apply your knowledge.

Homework is graded using a **9-point rubric**. The rubric has three
categories: submission, completeness, and correctness. Each category is
worth 3 points. By submitting an assignment on-time, with the structure
specified in the assignment (e.g., filenames, directories, method names,
etc.), you earn 3 points. By making an honest effort on each part of the
assignment you earn another 3 points. You earn 3 points in the last
category, correctness, based on the quality of your work and its
demonstration of your understanding of the material. Note that these
categories are not entirely independent of each other. If you skip a
significant part of the homework, you will lose points in both
completeness and correctness, because non-existent work is considered
incorrect work.

Notice that just by correctly submitting your work on time and making an
honest attempt on all parts will earn you no less than a 6/9 (66.66%) on the
assignment. From there the correctness of your work, as it demonstrates
your understanding of the material, determines the "letter grade" for that
assignment.


### Late Work

Unless otherwise specified, work is ***due by 9 AM ET*** on the day it is due.

If submitting homework late, the following policy applies.

* Less than 72 hours late - No penalty.
* More than 72 hours late but less than 1 week late - No more than 55.5%.
  Only completeness is checked, and no critical feedback given.
* More than 1 week late - 0.

Quizzes and exams must be completed on time, otherwise they receive a 0.
