### Extenuating Circumstances

If a circumstance beyond your control causes you to violate the above policies,
you may request special considerations for your circumstance. Make a request as
follows:

* Complete the Extenuating Circumstance "quiz" on Kodiak.
* AND contact the instructor by Discord or email.

Why 2 methods of contact? This ensures that if there is some technological
snafu (e.g., junk mail, wrong Discord server, Kodiak is down), there is
a much higher chance that I will get the message. Also, the Kodiak "quiz"
provides a formal way for me to track requests. However, it does not provide
a mechanism for 2-way communication. So it's still important to communicate
with me over Discord or eamil.

Requests must be submitted as soon as possible once you are aware of the
extenuating circumstances. Your instructor will determine the acceptability of
a request, along with the specific accommodations that will be made.

_If you have any problems with the above instructions, please discuss your
situation with your instructor._
