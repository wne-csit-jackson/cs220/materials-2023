# Homework 2 Instructions

* YOUR NAME
* THE DATE
* CS220-Spring-2023

## Instructions

1. Download and extract the zip that contains this file.
2. Edit this source of this file in VS Code.
   You can preview this file as a webpage by pressing `CTRL+k, v` (on
   Mac use `CMD` instead).
3. Write your answers below each question. Make sure that the question
   numbers are not changed as a result.
4. Save your work.
5. Zip up your edited copy of this file and any other files created as a
   result of this homework, and submit the zip to the designated Kodiak Assignment post.


## Part 1: Agile and Scrum

1. Watch the following videos on Agile and Scrum.

   * 4m Video: *Is Scrum Agile?* by Mark Shead. Dec 1, 2020. <https://youtu.be/gi4SOP2ElAw> .
   * 9m Video: *What is Scrum? (An Agile Cartoon)* by Mark Shead. Aug 20, 2020. <https://youtu.be/m5u0P1WPfvs>

   a. What important points should you take away?

   b. What questions are you left with?

2. There are many practices and sayings that are related to Agile. Some of
   them we'll be studying more in depth in this class, some not. In this
   question, you'll explore some of them and connect them back to Agile.
   That is, you need to (1) identify one or more Agile Principles that supports
   (or is supported by) the given practice or concept. Then (2) explain how so.
   You may not know one or more of the given concepts or practices. If that's
   the case, first do a little research on what it is. (This is exactly what
   you may need to do in the field when a coworker suggests incorporating
   a new practice.)

   a. Automated testing

   b. YAGNI

   c. Pair Programming

   d. Scrum - Identify 3 scrum practices, and the perform the same analysis for each.
      - `_____________`
      - `_____________`
      - `_____________`

## Specifically Scrum

# Scrum - Homework

1. Find a diagram that depicts the Scrum framework. Try to find one that matches
   the 2020 Scrum Guide. You'll know because it won't refer to things that are
   not in the 2020 Scrum Guide, and vice versa.
   Include the diagram as an image (e.g., png, gif, or jpeg).
   List any artifacts, roles, or events mentioned in the scrum guide that are
   not identified in the diagram or vice versa.


2. It's always good to find more resources on a topic.
   Find a video that explains in more detail how scrum works (preferable after 2020).
   Watch it. Briefly list points from the video that you either previously didn't know,
   or helped clarify for you. Provide a link to the video.


3. Complete the following assessment with a score of 90% or higher
   (you can retake it as many times as you like; consider trying for 100%):
   [Scrum Open Assessment](https://www.scrum.org/open-assessments/scrum-open).
   Print the result page as a PDF and add it to this folder.
