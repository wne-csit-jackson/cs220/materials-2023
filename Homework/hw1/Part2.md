## Part 2: Open-Source Licenses

Answer the following questions. Feel free to consult other sources to
help you answer these questions.

1. Which type of license ensures that all derivatives of the work will
   always have the same license?

2. Which type of license allows derivatives of the work to apply a
   different license to their new version?

3. Which type provides the most freedoms to recipients?

4. Which ensures that all recipients have the same rights on all
   derivations?

5. Which is easier for a company to work with because they do not have
   to regularly audit their commits to ensure they are in compliance
   with licenses?

6. Which does not require a contributor to supply a copyright notice
   and a statement of how their contribution is licensed?

7. MIT License (Expat), is a classic permissive license. GPLv3 (not
   LGPLv3) is a classic CopyLeft license. Look both up on
   [TLDRLegal](https://tldrlegal.com/). What makes the GPLv3 license a
   CopyLeft license?

8. What is Linux licensed under? What type of license is it?

9. Read the
    [Developer Certificate of Origin](https://developercertificate.org/).
    Linux requires its contributors to sign-off on this certificate for
    each contribution. What could go wrong if they didn't?

10. Read this
    [Contributor License Agreement](https://gist.github.com/pjcozzi/4d1ab2166519de7ba41b).
    Why must it mention the MIT license?

11. If you find code online that does not have a license, what can you
    do with it? Do you think that was the intent of the author? What can
    you do about it?
