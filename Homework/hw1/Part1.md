# Part 1: Setup Systems-Check

Complete each of the following questions on the computer that you will
be using for homework. By doing so, you are indicating to me that you
are ready to work on homework in this class using these tools.

You will be taking several screen shots. Please rename them as specified
without modifying the screen shot's original file extension. For example,
if the file had a `.png` extension before you rename it, it should have
a `.png` after you rename it.

1. Let's make sure you have a system on which to complete homework.
   On what system will you be using to complete your homework
   (e.g., personal laptop, a computer in Lab H201, etc.)?

2. Let's make sure you have Docker Desktop installed and running on that
   system. Open Docker Desktop and take a screen shot. Rename this file
   `docker` (keep its original file extension). Place this file in the
   directory for this assignment.

3. Let's make sure you have access to git and docker from the command-line.
   On Windows, open a git-bash terminal, on MacOS open Terminal or iTerm.
   You could also open a terminal from inside VS Code. Type the commands
   below. Take a screenshot of their output. Rename the file `terminal`
   (keep its original file extension). Place this file in the
   directory for this assignment.

    ```bash
    git --version
    docker --version
    ```

4. Now let's check that you can access your personal, private subgroup on
   GitLab for this class that will be used for future homework assignments.
   Your instructor should give you a link to where this should be.
   If they haven't, please ask them for it. Once you have it, open a Web
   browser, Log into GitLab, navigate to your personal, private subgroup
   on GitLab for this class. Take a screen shot, and name it `gitlab`
   (keep its original file extension).
   Place this file in the directory for this assignment.

5. Open VS Code and its extensions menu (the boxes icon in the left
   menu). Ensure that `Dev Containers` is installed and appears in
   the `INSTALLED` section of the extensions view. Take a screen shot
   showing this and name this file `vscode` (keep its original file extension).
   Place this file in the directory for this assignment.
