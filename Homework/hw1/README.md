# Homework 1 Instructions

* YOUR NAME
* THE DATE
* CS220-Spring-2023

## Instructions

1. Download hw1.zip. Unzip it.
    * [Zip/unzip on Windows](https://support.microsoft.com/en-us/windows/zip-and-unzip-files-f6dde0a7-0fec-8294-e1d3-703ed85e7ebc)
    * [Zip/unzip on Mac](https://support.apple.com/guide/mac-help/zip-and-unzip-files-and-folders-on-mac-mchlp2528/mac#:~:text=Unzip%20(expand)%20a%20compressed%20item,zip%20file.)

2. Open the unzipped folder in VS Code.

3. Work through each markdown file and write your answers in each file.

   * See [Markdown in Visual Studio Code](https://code.visualstudio.com/docs/languages/markdown) for help.
   * Write your answers below each question, making sure that the question
   numbers are not changed as a result (be sure to preview it).
   * If asked to save a screenshot, place the image file in the same folder containing this file.
   * Be sure to save your work as you go (CTRL+S or CMD+S).

4. Submit your work by zipping up your homework folder, and uploading the zip to the designated Kodiak Assignment post.

5. Check your work by downloading your submission to a different directory, extract it to a folder, open the folder in VS Code, and inspect that your answers and files are there.
