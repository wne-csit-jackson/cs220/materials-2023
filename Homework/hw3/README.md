# Homework - Git

## Part 0 - Setup and Submission

Download and extract the file that contains this zip.
For each part below, place the files related to that part within the
assignment folder.

When you are done, you'll zip up everything into a single zip and upload
it to the appropriate Kodiak assignment.

## Part 1 - Starting a new project

### Context

Pretend you are in a capstone class, and are going to be working in
a small team. Your team will be creating a mobile frontend for placing
orders to a Bear Necessities Market as part of the LibreFoodPantry community.
Specifically your team will be implementing an app that will run on Android.
At least one of your team members is running macOS on their personal machine,
and at least another is running Windows. Also, at least one team member
regularly uses use Vim and at least one prefers to use VisualStudioCode.
Since you'll be contributing to the LibreFoodPantry community, your project
will be licensed under GPLv3.0.

> **Important**
>
> Although the context for this part of the assignment is that of team,
> this assignment itself must be completed individually. That is, this
> is not a team assignment.

Create a new project folder named `part1` that will be the root of your
project. Place it under Git version control. Configure the project so
that files that are commonly generated during the
development of an Android app, including by the development environments
and operating systems your team is using, are not accidentally committed
to the repository. Also, configure your repository to make sure that line
endings of source files don't get messed up between macOS and Windows (tip:
Android apps are written in Java). Also, please place a `LICENSE` file in
the root of the project with the contents of the plaintext copy of the GPLv3.
Last, please place a `README.md` in the root that has the title of the project
and a link to the license file.

When you are done your repository should have the following files:

* README.md
* LICENSE
* .gitignore
* .gitattributes

### Constraints

1. You need at least 5 commits. The order of the commits do not matter.
2. Each commit should do one thing.
3. Use conventional commit messages.

### Resources

* [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
* [Collection of gitignore files](https://github.com/github/gitignore)
* [Collection of gitattributes files](https://github.com/alexkaratarakis/gitattributes)
* [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)


## Part 2 - Git Practice

Complete the following in the interactive tutorial [Learn Git Branching](https://learngitbranching.js.org/).

* Main tab - Introduction Sequence - 1-4
* Main tab - Ramping Up - 1-4
* Remote tab - Push & Pull -- Git Remotes! - 1-8

The main purpose of this part is to practice Git commands to manipulate
the graph that Git maintains. In particular, branching and merging. However
there are other concepts that we have not covered during class. These will
help support and improve your understanding of Git and how to make it do
what you want.

Take screen shots of the levels demonstrating that you have completed them.
Place this in a folder named `part2` in your assignment folder.

## Part 3 - Git Conflicts

The goal of this section is to get some practice with branching, merging,
and resolving conflicts.

1. Create a new folder named `part3` in your homework folder and place it
   under Git version control.
2. In that project, create a conflict. You'll need two branches.
   In each branch, modify the same lines in two different ways.
3. Then merge one branch into the other, causing a conflict.
4. Resolve the conflict and complete the merge.


## Submit and check your work

To submit your work, zip the assignment folder that contains all your work
and submit that zip to the appropriate assignment on Kodiak. If you forget
something and need to resubmit, add it to your assignment folder, and re-zip
the entire assignment folder and submit the new zip. This zip will replace
the last you submitted.

Check your work by downloading the zip you submitted, and extracting it to
a ***different location*** that the original. Inspect its contents, `git log`,
etc. An invalid submission is a quick way to get a 0.

> **Information**
>
> Why extract to a different location? First to make sure you don't destroy
> your original work. Second, if your zip was empty, you may be fooled into
> incorrectly believing that your existing work is what was extracted from
> the zip, when in fact the zip is empty.

---

Copyright 2022, Stoney Jackson <dr.stoney@gmail.com>. Licensed under CC-BY-SA 4.0.
