# Homework 5


Read the entire assignment before you begin.

## Goals

Gain experience with Clean Code.

## Part 1 - Select Code

Find some non-trivial code that you wrote in a previous course (data structures?).

- It doesn't have to be pretty. In fact, it's better if it is a bit rough.
- Ideally it runs and "works", but may have bugs.
- The language it's written in doesn't matter; as long as it is a standard
   high-level, 3rd generation programming language --- for example: Java, C,
   C++, Python, Javascript, etc.
- Is more than 2 files.
- One of the files is more than 50 lines.
- Preferably code that you wrote. If you didn't write it, each file must have
   the author(s) in comments at the top.
- You must understand the code.
- The code must allow you to complete the assignment below.

***If you can't do the above, let me know ASAP.***

## Part 2 - Prep

1. This file is README.md in the root of the project in cleancode-homework.zip.
   Download this zip and extract it.
2. Copy your code into both `./before/` and `./after/` and commit it.

Here on, you'll only modify the code in `./after/`.

## Part 3 - Review your code

1. Change your code based on the following videos from Clean Code:

   1. Names++
   2. Functions
   3. Function Structure
   4. Form

2. Write a report in `./report.md` that explains how you addressed one principle
   from each of the above videos.
   1. Your report must have 4 sections, one for each section (e.g., Names++, etc).
   2. In each section identify one principle that you applied/addressed.
      Identify the offending code in `./before`, and identify what I should
      look for and where in `./after`.

> **IMPORTANT**
>
> You must make at least one change related to each. Some principles overlap
> in section; you only get to count a principle once (i.e., no double-dipping).

## Submit your work

1. Commit your changes to your repository.
2. Zip the root of this project.
3. Upload this zip to the appropriate assignment post on Kodiak.

## Check your work

1. Download your zip from the assignment to a different location.
2. Extract it.
3. Confirm your work.

## If you need to fix your submission

1. Make and commit your changes.
2. Rezip the project.
3. Upload the new zip to the assignment post.
4. Check your work gain.

---
Copyright © 2023 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
