# GitHub/GitLab Workflow Homework

Use the fork-branch-merge-request workflow taught in
class to contribute changes to the following upstream project.

<https://gitlab.com/wne-csit-jackson/cs220/cs220-spring-2023/class/obmy>

This project's README.md provides guidelines about how to contribute changes to
it. Be sure to follow them as you complete the assignment below.

1. Report 5 issues.
2. Claim an issue. In a feature branch, fix the issue, and offer the change in
   a merge request.
3. Claim a second issue. In a separate feature branch, fix the issue, and offer
   the change in a separate merge request. Be sure to cut the new branch from
   main, and not the feature branch you created in the previous step.
4. Wait for a review by your instructor. One will be accepted (assuming you
   followed the contribution guidelines and your fix is correct). The other you
   will be asked to synchronize the feature branch associated with the merge
   request so that it contains the most recent changes in upstream main.


> **IMPORTANT DEADLINES**
>
> * Both merge-requests must be issued by ***Monday 4/3 at 9a (NO GRACE)***
> * Synchronizing of your merge request must be done by ***Monday 4/10 at 9a (grace ends at 9a 4/13)***.
