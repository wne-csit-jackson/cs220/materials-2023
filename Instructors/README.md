## Contents

This reposity contains files posted by the instructor.

* `Activities/` contains the source for activites that are posted to the wiki.
  Their URLs that point to this repository or the project's wiki need to be
  updated if this repository is copied to a new location. Each activity usually
  has two files, a template file (ending in `-Template`) and a landing page.
  To assign an activity, copy the landing page to the wiki and link it into
  the wiki's home page. I recommend placing them in a subdirectory within
  the wiki to avoid accidental overwrites. Below is the order of activities.

  1. Teams
  2. Markdown
  3. Software Setup

* `Homework/` - um, Homework.
* `Instructors/` - Documentation for instructors.

## Creating homework subgroups

Here is how I created each homework subgroup for each student in the class.

Create a csv file with the following fields

```csv
name,gitlab_user_name,url_to_student_subgroup
```

Create subgroups for each student under the homework group. (Get the group
id by navigating to the group on GitLab.)

```bash
HOMEWORK_GROUP_ID=#set this to the id of the group that will contain student subgroups
while read -r name uname url ; do glab api https://gitlab.com/api/v4/groups/ --raw-field path=$name --raw-field name=$name --raw-field parent_id=$HOMEWORK_GROUP_ID ; sleep 5 ; done < accounts.csv
```

URL encode the URL to each students subgroup.

```bash
while read -r name uname url ; do echo $name $uname $url $(python3 -c "import urllib.parse as p ; print(p.quote('$url', safe=''))") ; done < accounts.csv > accounts.txt
```

I manually removed `http://gitlab.com/` (encoded for URL) from the front of
the URL encode URL for each student.

Resolve user IDs and add them to our file.

```bash
while read -r name uname url eurl ; do echo $name $uname $url $eurl $(glab api /users/\?search=$uname | jq '.[0].id') ; done < accounts.txt > accounts2.txt
```

Grant permissions to each student over their homework subgroup.

```bash
while read -r name uname url grp uid ; do glab api /groups/$grp/members --raw-field user_id=$uid --raw-field access_level=40 --raw-field expires_at=2023-07-01 ; sleep 5 ; done < accounts2.txt
```
